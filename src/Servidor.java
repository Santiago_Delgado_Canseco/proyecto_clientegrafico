/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.net.*;
import java.io.*;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

// Se agregan las librerias para conectar con la BD
import java.sql.Connection;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;

/**
 *
 * @author gmendez
 */
public class Servidor {
    ArrayList<Conexion> conexiones; //<--Lista de conexiones
    ServerSocket   ss; // Encargado de conectar a usuarios
    Connection conn; //realizar la conexion exitosa a la base de datos
    String personas = ""; //Guardar a los usuarios en linea
    
    /*
    String [][] usuarios = {{"hugo",  "123"},
                            {"paco",  "345"},
                            {"luis",  "890"},
                            {"donald","678"}};
    */
    
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                (new Servidor()).start();
            }
        });
        
        
    }

    public String borrar(String nombre){//el nombre de quien seraborrado
        String pt1,pt2;
        pt1 = personas.substring(0,personas.indexOf(nombre));
        pt2 = personas.substring(personas.indexOf(nombre)+4,personas.length());
        return  pt1+pt2;
    }
    
    private void start() {
        this.conexiones = new ArrayList<>(); //inicializa
        Socket socket; //crea el socket
        Conexion cnx; // llama a una conexion
        
        try {
            // Se realiza una sola conexion
            if (conn == null){
                conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/aqui el nombre de la base de datos","su usuario","contraseña");//cambiar base de datos, usuario, contraseña
            }
            
            ss = new ServerSocket(4444);//ser la puerta de enlace a todas las conexiones
            System.out.println("Servidor iniciado, en espera de conexiones");
            
            while (true){             
                socket = ss.accept();//aceptar todas las conexiones entrantes
                cnx = new Conexion(this, socket, conexiones.size(),this.conn); //añadida a la lista de conexiones
                conexiones.add(cnx); //añade
                cnx.start(); //inicia el proceso          
            }            
            
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }  catch (SQLException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    // Broadcasting
    private void difundir(String id, String mensaje) {
        Conexion hilo;
        for (int i = 0; i < this.conexiones.size(); i++){
            hilo = this.conexiones.get(i);
            if (hilo.cnx.isConnected()){
                if (!id.equals(hilo.id)){//quien no envio el mensaje
                    hilo.enviar(mensaje);//recibe el mensaje quien no lo envio
                }
            }
        }//To change body of generated methods, choose Tools | Templates.
    }
    
    class Conexion extends Thread {
        BufferedReader in;
        PrintWriter    out;
        Connection     conn;
        Socket         cnx;
        Servidor       padre;
        int            numCnx = -1;
        String         id = "";
        
        public final int SIN_USER   = 0; //No usuario
        public final int USER_IDENT = 1; //Identificar usuario
        public final int PASS_PDTE  = 2; //Identificar password
        public final int PASS_OK    = 3; //Login compeado
        public final int CHAT       = 4; //estado final
                
        public Conexion(Servidor padre, Socket socket, int num, Connection _conn){
            this.conn = _conn;
            this.cnx = socket;
            this.padre = padre;
            this.numCnx = num;
            this.id = socket.getInetAddress().getHostAddress()+"-"+num;
        }
        
        @Override
        public void run() {
            String linea="", user="", pass="", mensaje="";
            String passValido = "";
            int estado = SIN_USER;
            int usr = -1;
            int intentos = 3;
            
            Statement stmt;
            ResultSet rset;

            String    sQuery = "";
            
            try {
                in = new BufferedReader(new InputStreamReader(cnx.getInputStream()));
                out = new PrintWriter(cnx.getOutputStream(),true);
                
                System.out.printf("Aceptando conexion desde %s\n",
                        cnx.getInetAddress().getHostAddress());
                                
                while(!mensaje.toLowerCase().equals("salir") && intentos>0){//salir saca del ciclo                    
                    switch (estado){
                        case SIN_USER:
                            out.println("Bienvenido, proporcione su usuario");//solicitaos el usuario
                            estado = USER_IDENT;
                            break;
                        case USER_IDENT:
                            user = in.readLine();//leeemos el usuario que el cliente envias
                            boolean found = false; //creamos que diga que aun no se ha encontrado 
                            passValido = "";
                            sQuery = "SELECT usuario,password FROM usuarios WHERE usuario = '"+user.trim()+"'";
                            
                            try {
                                stmt = conn.createStatement();
                                rset = stmt.executeQuery(sQuery);
                                
                                if (rset.next()){//recorremos resultados
                                    found = true;//encontramos usuario
                                    passValido = rset.getString("password");//Extraemos el password
                                }
                                
                            } catch (SQLException sqle){
                                
                            }
                            
                            if (!found){
                                estado = SIN_USER;
                            } else {
                                estado = PASS_PDTE;
                            }                                                                                                             
                            break;
                        case PASS_PDTE:
                            out.println("Escriba el password");//enviamos a que nos manden el password
                            pass = in.readLine();//leeemos el pasword                    
                            if (pass.equals(passValido)){
                                estado = PASS_OK;
                                personas = personas + user;//añadimos esta persona a la lista personas en la conexion
                            }
                            --intentos;
                            break;
                        case PASS_OK:
                            out.println("Autenticado!");
                            out.println(personas);//lista de las personas en linea
                            difundir(id,user+"Entro!.");//el cliente entro
                             difundir(id,user);//el cliente
                            estado = CHAT;
                            break;
                        case CHAT:
                            mensaje = in.readLine();
                            System.out.printf("%s - %s\n",
                                        cnx.getInetAddress().getHostAddress(),
                                        user + ":" +mensaje);
                            
                            this.padre.difundir(this.id, this.id+" | "+user+" : "+mensaje);
                            break;
                    }                        
                }            
                this.padre.difundir(id, user);//difundimos el nombre del usuario que salio
                borrar(user);//borrar el usuario
                this.cnx.close();//cerramos la conexion
                System.out.println("Cerrando conexion: "+this.id);
            } catch (IOException ex) {
                Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
            }            
        }

        private void enviar(String mensaje) {
            this.out.println(mensaje); //To change body of generated methods, choose Tools | Templates.
        } 
    }   
}
